﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.MediaManager;
using Plugin.MediaManager.Abstractions;
using Plugin.MediaManager.Abstractions.Enums;
using Plugin.MediaManager.Abstractions.EventArguments;
using Plugin.MediaManager.Abstractions.Implementations;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vishista.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Vishista : ContentPage
	{
        private bool isPlaying = false;

        public Vishista ()
		{
			InitializeComponent ();
            //this.volumeLabel.Text = "Volume (0-" + CrossMediaManager.Current.VolumeManager.MaxVolume + ")";
            ////Initialize Volume settings to match interface
            //int.TryParse(this.volumeEntry.Text, out var vol);
            //CrossMediaManager.Current.VolumeManager.CurrentVolume = vol;
            ////CrossMediaManager.Current.VolumeManager.Muted = false;

            CrossMediaManager.Current.PlayingChanged += (sender, e) =>
            {
                Debug.WriteLine($"PlayingChanged Position: {e.Position.TotalSeconds}  Duration: {e.Duration.TotalSeconds}  Progress: {e.Progress * 100.0}");
                //ProgressBar.Value = e.Progress * 100.0;
                //Duration.Text = "" + e.Duration.TotalSeconds + " seconds";
            };
        }

	    private void CurrentOnStatusChanged(object sender, StatusChangedEventArgs e)
	    {
	        
        }


        private async void OnPlayClicked(object sender, System.EventArgs e)
	    {
            if (!isPlaying)
            {
                isPlaying = true;
                //btnPlay.Source = ImageSource.FromFile("pause_button.png");
                var mediaFile = new MediaFile
                {
                    Type = MediaFileType.Audio,
                    Availability = ResourceAvailability.Remote,
                    Url = "https://audioboom.com/posts/5766044-follow-up-305.mp3",
                };
                await CrossMediaManager.Current.Play(mediaFile);
            }
            else
            {
                isPlaying = false;
                //btnPlay.Source = ImageSource.FromFile("play_button.png");
                await CrossMediaManager.Current.Stop();
            }
        }
    }
}