﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vishista.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class More : ContentPage
	{
		public More ()
		{
			InitializeComponent ();
		}

	    async void OnButtonClicked(object sender, EventArgs args)
	    {
	        await Navigation.PushAsync(new Bhajans(), true);
	    }
    }
}