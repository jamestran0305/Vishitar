﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Vishista.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Bhajans : ContentPage
	{
	    private string[] data = new[]
	    {
	        "All Bhajans",
	        "Jai SrimanNarayana",
	        "Adisesha Anantasayana",
	        "Sri:niva:sa Go:vinda:",
	        "Go:vinda Jai Jai Go:pa:la Jai Jai",
	        "Hari Bol Hari Bol",
	        "Hari Na:ra:yana Go:vinda",
	        "He Murali Ma:dhava",
	        "Jaya Jaya Ra:ma Ja:naki Ra:ma",
	        "Krishna Krishna Mukunda Jana:rdana",
	        "Krishna:nanda Mukunda Mura:re:",
	        "Bhaja Yathirajam",
	        "Govinda Hare: Go:pala Hare:",
	        "Nya:ya Dharma Pa:laka Lo:ka",
	        "Om Namo Bhagavathe Va:sudeva:ya",
	        "Radhe: Radhe: Radhe: Radhe: Ra:dhe Govinda:",
	        "Sya:malam Ko:malam Ra:ma",
	        "Govinda Nama"
	    };

		public Bhajans ()
		{
			InitializeComponent ();
            Array.Sort(data);
		    BindingContext = data;
		}

	    async void OnTapped(object sender, EventArgs e)
	    {
	        await Navigation.PushAsync(new Vishista(), true);
        }
    }
}